require("dotenv").config();
const { schedule } = require("node-cron");
const logger = require("./logger");
const { HubSpotList } = require("./hubspot");
const landlordData = require("./landlordData.json")["landlords"];
const fs = require("fs");
const _ = require("lodash");
const KEY = "47e6801e-26f5-4a46-96f2-a7244c59d3ae";
//
let start = 0;
let end = 10;
// every  midnight cron
schedule("0 0 * * *", async () => {
  try {
    // check if file exists
    fs.readFile("landlordData.json", (err, data) => {
      if (!err && data) {
        // ...
        fs.unlinkSync("landlordData.json");
      }
    });

    let NewLandlordData = await fetch(
      `https://landlord.inlifehousing.com/api/external/getAllLandlordsHubspot?key=${KEY}`,
      {
        method: "GET",
      }
    )
      .then((response) => response.json())
      .then();
    if (!_.isEmpty(NewLandlordData)) {
      // STEP 3: Writing to a file
      fs.writeFile(
        "landlordData.json",
        JSON.stringify(NewLandlordData),
        (err) => {
          // Checking for errors
          if (err) throw err;

          console.log("Done writing"); // Success
          start = 0;
          end = 10;
        }
      );
    }
  } catch (err) {
    logger.error(err);
  }
});

// every 2 minutes cron
schedule("*/2 * * * *", () => {
  if (!_.isEmpty(landlordData)) {
    let FinalData = landlordData.slice(start, end);
    HubSpotList(FinalData);
    start = end;
    end = end + 10;
  }
});
