require("dotenv").config();
const hubspot = require("@hubspot/api-client");
const logger = require("./logger");
const request = require("request");
const _ = require("lodash");
const TOKEN = "pat-eu1-b2050385-aa9f-4ceb-9063-37e90b02ede7";

// date convert
async function toUNIX(isoDate) {
  try {
    if (_.isEmpty(String(isoDate))) {
      return "";
    }
    var dateISO;
    // Create date object
    var d = new Date(isoDate);
    // Set to midnight
    d.setUTCHours(0, 0, 0, 0);
    // Convert to ISO
    dateISO = d.toISOString();
    let FinalDate = Date.parse(dateISO);
    // Create unix timestamp using Date.parse()
    return FinalDate;
  } catch {
    return "";
  }
}

// get country name from phone number
async function getCountryName(country) {
  if (_.isArray(country)) {
    country[0] =
      String(country[0]).charAt(0).toUpperCase() + country[0].slice(1);
    const countryList = ["Spain", "Portugal", "Italy"];
    if (countryList.includes(country[0])) {
      return country[0];
    }
    return "Others";
  } else {
    return "Others";
  }
}

// get company list from Hubspot
async function isListedData(email) {
  let response = await fetch(
    `https://api.hubapi.com/contacts/v1/contact/email/${email}/profile`,
    {
      headers: {
        Authorization: `Bearer ${TOKEN}`,
      },
    }
  )
    .then((res) => res.json())
    .then();

  return response;
}

// create company
async function createCompany(hubspotData) {
  _.each(hubspotData, async (data, idx) => {
    try {
      const listedData = await isListedData(data?.email);
      // let ans = response.results.find((dd) =>  dd.properties.domain == 'erasmusplay.com');
      // console.log("🚀 ~ file: hubspot.js:247 ~ listCompapies ~ ans:", ans)
      if (listedData.message == "contact does not exist") {
        // create company
        var options = {
          method: "POST",
          url: "https://api.hubapi.com/crm/v3/objects/companies",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${TOKEN}`,
          },
          body: JSON.stringify({
            properties: {
              domain: String(
                `${data.firstName}${data.lastName}.com`
              ).toLowerCase(),
              name: `${
                String(data?.firstName).charAt(0).toUpperCase() +
                String(data?.firstName).slice(1)
              } ${
                String(data?.lastName).charAt(0).toUpperCase() +
                String(data?.lastName).slice(1)
              }`,
              commission: data?.commission / 100,
              backoffice_id: data?.id,
              date_of_first_active_listing: Date.parse(
                data?.firstActivelistingDate
              ),
              date_of_last_listing_posted: await toUNIX(
                Date.parse(data?.lastListingPostedDate)
              ),
              number_of_ads_in_progress: data?.adsInProgressNumber,
              number_of_inactive_ads: data?.inactiveAdsNumber,
              number_of_active_ads: data?.activeAdsNumber,
              date_of_first_active_listing: Date.parse(
                data?.firstBookingRequestDate
              ),
              date_of_last_booking_request: Date.parse(
                data?.lastBookingRequestDate
              ),
              total_number_of_confirmed_bookings: data?.confirmedBookingsNumber,
              date_of_first_acceptance: Date.parse(data?.firstAcceptanceDate),
              date_of_last_acceptance: Date.parse(data?.lastAcceptanceDate),
              acceptance_rate: data?.acceptanceRate,
            },
          }),
        };
        request(options, function (error, response) {
          if (error) throw new Error(error);
          let responseData = JSON.parse(response.body);

          if (!_.isEmpty(String(responseData.id))) {
            console.log(
              "🚀 ~ file: hubspot.js:143 ~ String(responseData.id):",
              String(responseData.id)
            );
            // insertCompany(data.id, responseData.id);
            createContact(responseData.id, data);
          }
        });
      } else {
        // update company
        // listedData.vid => contactId
        // listedData['associated-company']['company-id'] => associated company
        if (
          listedData &&
          listedData["associated-company"] &&
          listedData["associated-company"]["company-id"]
        ) {
          await updateComapny(
            listedData["associated-company"]["company-id"],
            data
          );
          await updateContact(
            listedData.vid,
            data,
            listedData["associated-company"]["company-id"]
          );
        }
      }
      if (
        listedData.vid &&
        listedData["associated-company"] &&
        listedData["associated-company"]["company-id"]
      ) {
        await updateContact(
          listedData.vid,
          data,
          listedData["associated-company"]["company-id"]
        );
      }
    } catch (err) {
      console.log("🚀 ~ file: hubspot.js:128 ~ _.each ~ err:", err);
      logger.error(err);
    }
  });
}

// update compnay
async function updateComapny(companyID, data) {
  try {
    // create company
    var options = {
      method: "PATCH",
      url: `https://api.hubapi.com/crm/v3/objects/companies/${companyID}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${TOKEN}`,
      },
      body: JSON.stringify({
        properties: {
          domain: String(`${data.firstName}${data.lastName}.com`).toLowerCase(),
          name: `${
            String(data?.firstName).charAt(0).toUpperCase() +
            data?.firstName.slice(1)
          } ${
            String(data?.lastName).charAt(0).toUpperCase() +
            data?.firstName.slice(1)
          }`,
          commission: data?.commission / 100,
          backoffice_id: data?.id,
          date_of_first_active_listing: Date.parse(
            data?.firstActivelistingDate
          ),
          date_of_last_listing_posted: Date.parse(data?.lastListingPostedDate),
          number_of_ads_in_progress: data?.adsInProgressNumber,
          number_of_inactive_ads: data?.inactiveAdsNumber,
          number_of_active_ads: data?.activeAdsNumber,
          date_of_first_active_listing: Date.parse(
            data?.firstBookingRequestDate
          ),
          date_of_last_booking_request: Date.parse(
            data?.lastBookingRequestDate
          ),
          total_number_of_confirmed_bookings: data?.confirmedBookingsNumber,
          date_of_first_acceptance: Date.parse(data?.firstAcceptanceDate),
          date_of_last_acceptance: Date.parse(data?.lastAcceptanceDate),
          acceptance_rate: data?.acceptanceRate,
        },
      }),
    };
    request(options, async function (error, response) {
      if (error) throw new Error(error);
      let responseData = JSON.parse(response.body);
      console.log(`idx:: updateComapny`, responseData);
      if (responseData.id) {
        await updateContact(responseData.id, data, companyID);
      }
    });
  } catch (error) {
    console.log("🚀 ~ file: hubspot.js:182 ~ updateComapny ~ error:", error);
    logger.error(error);
  }
}

// create contact
async function createContact(companyID, data) {
  try {
    // _.each(SplitData, async (data, idx) => {
    const listedData = await isListedData(data?.email);
    if (listedData.message == "contact does not exist") {
      // create contact
      var options = {
        method: "POST",
        url: "https://api.hubapi.com/crm/v3/objects/contacts",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${TOKEN}`,
        },
        body: JSON.stringify({
          properties: {
            associatedcompanyid: companyID,
            company: "",
            email: data?.email,
            firstname:
              String(data?.firstName).charAt(0).toUpperCase() +
              String(data?.firstName).slice(1),
            lastname:
              String(data?.lastName).charAt(0).toUpperCase() +
              String(data?.lastName).slice(1),
            phone:
              data?.phoneNumber == null || _.isEmpty(data?.phoneNumber)
                ? ""
                : data?.phoneNumber,
            last_login: await toUNIX(data?.lastLogin),
            registration_date: await toUNIX(Date.parse(data?.registrationDate)),
            temporary_country: !_.isEmpty(data?.countries)
              ? await getCountryName(data?.countries)
              : "Others",
            date_of_first_response: await toUNIX(
              Date.parse(data?.firstResponseDate)
            ),
            date_of_last_response: await toUNIX(
              Date.parse(data?.lastResponseDate)
            ),
            response_rate: data?.responseRateAverage,
          },
        }),
      };
      request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(`idx::`, response.body);
        let responseData = JSON.parse(response.body);
      });
    }
  } catch (error) {
    console.log("🚀 ~ file: hubspot.js:226 ~ //_.each ~ error:", error);
    logger.error(error);
  }
  // });
}

// update contact
async function updateContact(contactID, data, companyId) {
  try {
    var options = {
      method: "PATCH",
      url: `https://api.hubapi.com/crm/v3/objects/contacts/${contactID}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${TOKEN}`,
      },
      body: JSON.stringify({
        properties: {
          associatedcompanyid: companyId,
          company: "",
          email: data?.email,
          firstname:
              String(data?.firstName).charAt(0).toUpperCase() +
              String(data?.firstName).slice(1),
            lastname:
              String(data?.lastName).charAt(0).toUpperCase() +
              String(data?.lastName).slice(1),
          phone:
            data?.phoneNumber == null || _.isEmpty(data?.phoneNumber)
              ? ""
              : data?.phoneNumber,
          last_login: await toUNIX(data?.lastLogin),
          registration_date: await toUNIX(Date.parse(data?.registrationDate)),
          temporary_country: !_.isEmpty(data?.countries)
            ? await getCountryName(data?.countries)
            : "Others",
          date_of_first_response: Date.parse(data?.firstResponseDate),
          date_of_last_response: Date.parse(data?.lastResponseDate),
          response_rate: data?.responseRateAverage,
        },
      }),
    };
    request(options, function (error, response) {
      if (error) throw new Error(error);
      console.log(`idx:: updateContact`, response.body);
      let responseData = JSON.parse(response.body);
    });
  } catch (error) {
    console.log("🚀 ~ file: hubspot.js:267 ~ updateContact ~ error:", error);
    logger.error(error);
  }
}

module.exports = {
  HubSpotList: function (data) {
    createCompany(data);
  },
};
